#include <iterator>
#include <iostream>
#include <fstream>

#include <Poco/Net/HTTPServer.h>
#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>
#include <Poco/Util/ServerApplication.h>
#include <Poco/StreamCopier.h>
#include <Poco/File.h>

#include "FileServer.h"

using namespace Poco::Net;
using namespace Poco::Util;
using namespace std;

class FileServer::Impl: public enable_shared_from_this<FileServer::Impl> {
public:
    using TokenFileMap = std::map<
            std::string,
            Poco::Path
    >;

    TokenFileMap tokenFileMap;


    class FileUploadHandler : public HTTPRequestHandler
    {
        Impl* self_;
    public:
        FileUploadHandler(Impl* self): self_(self) {}

        virtual void handleRequest(HTTPServerRequest &req, HTTPServerResponse &resp)
        {
            if(req.getMethod() != "GET") {
                resp.setStatus(HTTPResponse::HTTP_REASON_BAD_REQUEST);
                resp.setReason("Bad request");
                return;
            }

            std::string uri = req.getURI();
            std::string token = uri.substr(1);

            TokenFileMap::iterator fileRecord = self_->tokenFileMap.find(token);

            if(fileRecord == self_->tokenFileMap.end()) {
                resp.setStatus(HTTPResponse::HTTP_NOT_FOUND);
                resp.send() << "Cannot find a file with given token: " + token;
                return;
            }

            const auto& record = *fileRecord;

            std::string path = record.second.absolute().toString();

            ifstream fileStream(path, ios_base::binary);

            if(!fileStream.is_open()) {
                resp.setStatus(HTTPResponse::HTTP_NOT_FOUND);
                resp.send() << "Requested file no longer exists";
                return;
            }

            try {
                resp.setStatus(HTTPResponse::HTTP_OK);
                resp.setChunkedTransferEncoding(true);
                resp.setContentType("application/octet-stream");
                resp.set("Content-Disposition", "attachment; filename=\"" + record.second.getFileName() + "\"");

                ostream& out = resp.send();
                Poco::StreamCopier::copyStream(fileStream, out);

                out.flush();
            }
            catch (std::exception& e) {
                cerr << "Error happened during uploading '" << path << "' : " << e.what() << endl;
            }
        };
    };

    class FSRequestHandlerFactory : public HTTPRequestHandlerFactory
    {
        Impl* self_;
    public:
        FSRequestHandlerFactory(Impl* self): self_(self) {}

        virtual HTTPRequestHandler* createRequestHandler(const HTTPServerRequest &)
        {
            return new FileUploadHandler(self_);
        }
    };

    HTTPServer server_;
    bool isRunning = false;

    Impl(const Poco::Net::ServerSocket &socket): server_(
            new FSRequestHandlerFactory(this),
            socket,
            new HTTPServerParams) {
    }

    bool shareFile(Poco::Path p, std::string token) {
        Poco::File test(p);
        if(!test.exists() || !test.isFile())
            return false;

        tokenFileMap.insert({std::move(token), std::move(p)});
        return true;
    }

    void run() {
        if(isRunning)
            return;

        isRunning = true;
        server_.start();
    }

    void stop() {
        if(!isRunning)
            return;

        isRunning = false;

        server_.stop();
    }
};
void FileServer::run() {
    pImpl->run();
}

bool FileServer::shareFile(Poco::Path p, std::string token) {
    return pImpl->shareFile(std::move(p), std::move(token));
}

FileServer::FileServer(const Poco::Net::ServerSocket &socket)
    : pImpl(make_unique<Impl>(socket)) { }

void FileServer::stop() {
    pImpl->stop();
}

FileServer::~FileServer() {
    stop();
}
