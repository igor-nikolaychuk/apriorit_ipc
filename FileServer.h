#ifndef APRIORIT_IPC_FILESERVER_H
#define APRIORIT_IPC_FILESERVER_H

#include <memory>

#include <Poco/Path.h>
#include <Poco/Net/ServerSocket.h>

class FileServer {
    class Impl;
    std::shared_ptr<Impl> pImpl;
public:
    FileServer(const Poco::Net::ServerSocket& socket);
    void run();
    void stop();
    bool shareFile(Poco::Path p, std::string token);
    ~FileServer();;
};

#endif //APRIORIT_IPC_FILESERVER_H
