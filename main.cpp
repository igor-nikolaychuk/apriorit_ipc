#include <iostream>
#include <thread>
#include <fstream>
#include <regex>

#include "FileServer.h"

#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/StreamCopier.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/File.h>
#include <Poco/Util/ServerApplication.h>

using namespace Poco::Net;

int main(int argc, const char* args[]) {
    if(argc != 2) {
        std::cout << "Usage: \nfile_server file_to_share" << std::endl;
        return -1;
    }

    const uint16_t SERVER_PORT = 8080;

    Poco::Net::ServerSocket socket(SERVER_PORT);
    FileServer fileServer(socket);

    std::string path(args[1]);

    std::string fileToken = "testfile";

    if(!fileServer.shareFile(path, fileToken)) {
        std::cerr << "Unable to find given file" << std::endl;
        return -1;
    }
    else {
        std::cout << "File can be downloaded from: "
                  "http://127.0.0.1:" << SERVER_PORT << "/" << fileToken << std::endl;
    }

    fileServer.run();

    //Downloading the file

    HTTPClientSession session("127.0.0.1", 8080);
    HTTPRequest req(HTTPRequest::HTTP_GET, "/testfile", HTTPMessage::HTTP_1_1);
    req.setChunkedTransferEncoding(true);
    session.sendRequest(req);

    HTTPResponse res;
    std::istream& rs = session.receiveResponse(res);
    {
        //Read Content-Disposition header to get original file name
        std::string contentDisp = res.get("Content-Disposition");
        std::smatch match;

        std::string readFileName = "out";

        std::regex filenameRgx("filename=\\\"(.+?)\\\"");
        if(std::regex_search(contentDisp, match, filenameRgx))
            readFileName = match[1];

        size_t i = 0;

        std::string fileName;

        //File with given name may already exist in current directory
        do {
            fileName = (
                           i++ ?
                             "_" + std::to_string(i) + "_" :
                             ""
                       ) + readFileName;
        }
        while (Poco::File(fileName).exists());


        std::cout << "Downloading to " << fileName << std::endl;

        try {
            std::ofstream output(fileName);
            auto read = Poco::StreamCopier::copyStream(rs, output);
            std::cout << "Done! " << read << " bytes read." << std::endl;
        }
        catch (const std::exception& e) {
            std::cerr << "Error happened when downloading: "
                      << e.what() << std::endl;
        }
    }

    std::cout << "Press enter key to stop file server" << std::endl;
    std::cin.ignore();

    return 0;
}
